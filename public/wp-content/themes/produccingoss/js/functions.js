/************************************************************
 ** Clears a field
 ** By: 	Joshua Sowin (fireandknowledge.org)
 ** HTML: <input type="text" value="Search" name="search"
 **			id="search" size="25"
 ** 		onFocus="clearInput('search', 'Search')"
 ** 		onBlur="clearInput('search', 'Search')" />
 ***********************************************************/
function clearInput(field_id, term_to_clear) {

	// Clear input if it matches default value
	if (document.getElementById(field_id).value == term_to_clear ) {
		document.getElementById(field_id).value = '';
	}

	// If the value is blank, then put back term
	else if (document.getElementById(field_id).value == '' ) {
		document.getElementById(field_id).value = term_to_clear;
	}
} // end clearSearch()

/**
 * Send by email
 */
function sendbyemail(title,url) {
  window.location="mailto:insert_here@your_friend.mail?subject=Olla este artigo '"+title+"' de Poss-gl&body=Por favor visita esta interesante páxina web, "+url+" desde o sitio web do Proxecto de Tradución ao Galego poss-gl.";
}

jQuery(document).ready(function(){

  $lock=false;
  jQuery("div.actions").hover(
    function () {
      if (!$lock){
        $lock=true;
        jQuery(this).children("ul").fadeIn("fast");
      }
      $lock=false;
    },
    function () {
      if (!$lock){
        $lock=true;
        jQuery(this).children("ul").fadeOut("fast");
      }
      $lock=false;
    }
  );

jQuery.fn.fadeToggle = function(speed, easing, callback) {
   return this.animate({opacity: 'toggle'}, speed, easing, callback);
};
  setInterval(function() {
    jQuery('#teaser-0').fadeToggle('slow', "linear", function(){
      jQuery('#teaser-1').fadeToggle('slow');
    });
  },10000);


});
